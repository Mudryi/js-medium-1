class SquareGenerator {
  constructor(wrapElementSelector = '.square-generator', squaresCount = 100) {
    this.wrapElementSelector = wrapElementSelector;
    this.squaresCount = squaresCount;
    this.squareHtmlTemplate = "<div data-click-value='0' data-square-color='#FFFFFF' class='square'><span class='clicks-count'></span></div>";
  }

  /* Squares initialization */

  /* Generate and insert to wrapper html of squares */
  createSquares() {
    let squaresHtml = '';
    for (let i = 0; i < this.squaresCount; i += 1) {
      squaresHtml += this.squareHtmlTemplate;
    }
    document.querySelector(this.wrapElementSelector).innerHTML = squaresHtml;
  }

  /* Delegate click on wrapper to target child element */
  addSquaresClickEvent() {
    const wrapperNode = document.querySelector(this.wrapElementSelector);

    wrapperNode.addEventListener('click', (event) => {
      let currentNode = event.target;

      while (!wrapperNode.isEqualNode(currentNode)) {
        if (currentNode.classList.contains('square')) {
          this.handleSquareClick(currentNode);
          currentNode = wrapperNode;
        } else {
          currentNode = currentNode.parentElement;
        }
      }
    });
  }

  /* Update data attribute values */
  handleSquareClick(squareElement) {
    const localSquareElement = squareElement;
    let squareColor = '';
    let clickValue = squareElement.dataset.clickValue;

    localSquareElement.dataset.clickValue = ++clickValue;

    switch (true) {
      case (clickValue <= 25):
        squareColor = '#FFFFFF';
        break;

      case (clickValue <= 50):
        squareColor = '#FCF6A9';
        break;

      case (clickValue <= 75):
        squareColor = '#FCCF05';
        break;

      case (clickValue <= 100):
        squareColor = '#FC8505';
        break;

      case (clickValue > 100):
        squareColor = '#F50202';
        break;

      default:
        squareColor = '#FFFFFF';
    }
    localSquareElement.dataset.squareColor = squareColor;
  }


  /* Manage buttons initialization */

  /* Generate and insert to wrapper html of manage buttons */
  createManageButtons() {
    const manageBtnsHtml = `<div class='manage-buttons'>
                                <button class='generate-clicks'>Generate click</button>
                                <button class='show-clicks'>Show results</button>
                                <button class='reset-clicks'>Reset</button>
                              </div>`;

    document.querySelector(this.wrapElementSelector).insertAdjacentHTML('beforeend', manageBtnsHtml);
  }

  addManageButtonsEvents() {
    const squares = document.querySelectorAll('.square');
    const squaresCount = this.squaresCount;

    /* Generate random clicks */
    document.querySelector('.generate-clicks').addEventListener('click', () => {
      for (let i = 0; i < squaresCount; i += 1) {
        squares[Math.floor(Math.random() * squaresCount)].click();
      }
    });

    /* Show clicks and color */
    document.querySelector('.show-clicks').addEventListener('click', () => {
      for (let i = 0; i < squaresCount; i += 1) {
        squares[i].querySelector('.clicks-count').innerText = squares[i].dataset.clickValue;
        squares[i].style.backgroundColor = squares[i].dataset.squareColor;
      }
    });

    /* Reset all values */
    document.querySelector('.reset-clicks').addEventListener('click', () => {
      for (let i = 0; i < squaresCount; i += 1) {
        squares[i].dataset.clickValue = '';
        squares[i].dataset.squareColor = '#FFFFFF';
      }
      document.querySelector('.show-clicks').click();
    });
  }

  initApp() {
    /* Init squares */
    this.createSquares();
    this.addSquaresClickEvent();

    /* Init manage buttons */
    this.createManageButtons();
    this.addManageButtonsEvents();
  }
}

/* Init app */
const generator = new SquareGenerator();
generator.initApp();
